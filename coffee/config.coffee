@SmoosterExtra or (@SmoosterExtra = {})
jQuery.extend @SmoosterExtra,
  config:
  		smo_glide_categories_admin:
          element: $('[data-smo-categories]')
          list_items: 'li'
          categories: ['kampagnen', 'print', 'corporate', 'digital']
          after_add: ->
            console.log "after add callback"

  debug: false
